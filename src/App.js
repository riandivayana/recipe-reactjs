import React from 'react';
import logo from './logo.svg';
import './App.css';

class App extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      name: "",
      email: "",
      phone: "",
      password: "",
      passwordConfirmation: ""
    }
  }

  doLogin = () => {
    console.log('run')
    console.log(JSON.stringify({
      name: this.state.name,
      email: "rian.divayana@gmail.com",
      phone: "0812312312",
      password: "dweaweaw"
    }))
    fetch('https://cors-anywhere.herokuapp.com/' + 'https://recipe.timedoor.id/api/v1/register', {
      method: 'POST',
      crossDomain: true,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name: this.state.name,
        email: this.state.email,
        phone: this.state.phone,
        password: this.state.password,
        password_confirmation: this.state.passwordConfirmation
      })
    })
    .then((respone) => respone.json())
    .then((responseJson) => {
      alert(JSON.stringify(responseJson))
    })
    .catch((error) => {
      console.error(error)
    })
  }

  render() {
    return (
      <div style={appStyle}>
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <input 
            type="text" 
            placeholder="Name"
            style={inputText}
            value={this.state.name}
            onChange={(text) => this.setState({name: text.target.value})}/>
          <input 
            type="email" 
            placeholder="Email"
            style={inputText}
            value={this.state.email}
            onChange={(text) => this.setState({email: text.target.value})}/>
          <input 
            type="text" 
            placeholder="Phone"
            style={inputText}
            value={this.state.phone}
            onChange={(text) => this.setState({phone: text.target.value})}/>
          <input 
            type="password" 
            placeholder="Password"
            style={inputText}
            value={this.state.password}
            onChange={(text) => this.setState({password: text.target.value})}/>
          <input 
            type="password" 
            placeholder="Password Confirmation"
            style={inputText}
            value={this.state.passwordConfirmation}
            onChange={(text) => this.setState({passwordConfirmation: text.target.value})}/>
          <button
            style={{width: 300, marginTop: 20, height: 50, borderRadius: 10}}
            onClick={this.doLogin}>Register</button>
        </div>
      </div>
    );
  }

}

const appStyle = {
   textAlign: 'center'
}

const inputText = {
  width: 300,
  height: 20,
  borderRadius: 5,
  paddingHorizontal: 5,
  marginHorizontal: 100, 
  marginBottom: 16, 
  justifyContent:'center'
}


export default App;
